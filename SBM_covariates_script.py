import numpy as np
import scipy.linalg as la
# import pandas as pd
import scipy.stats as stats
import time
import cPickle as pickle
# import matplotlib.pyplot as plt
# % matplotlib inline
# import seaborn as sns
# sns.set(style='white', palette = 'colorblind', color_codes=True)



# functions to create synthetic datasets

def sbm_adjacency(nA, dA, lamA):
    v = np.ones(nA)
    v[0:int(np.floor(nA/2))] =-1.0
    expectedA = np.ones((nA, nA))*(dA/nA) + lamA*np.sqrt(dA)*np.outer(v, v)/nA
    unifs = np.tril(np.random.rand(nA, nA))
    unifs = unifs + unifs.T
    A = (expectedA> unifs)
    A = A.astype(float)
    A = A - np.diag(np.diag(A))
    
    return A, v


def sbm_covariates(nB, pB, vB, muB):
    # nB should be the length vB
    u = np.random.randn(pB)
    B = np.sqrt(muB/(nB*pB))*np.outer(u, vB) + np.random.randn(pB, nB)/np.sqrt(pB)
    return B, u


# functions to create test matrices from synthetic data

def nonbacktracking_from_adj(nbA):
    # input is symmetric zero one adjacency matrix. make it sparse.
    row_inds, col_inds = np.nonzero(nbA) # this gets both directed and undirected edges
    n_dedges = len(row_inds)
    NBW = np.zeros((n_dedges, n_dedges))
    for d_edge in range(n_dedges):
        tail = col_inds[d_edge]
        head = row_inds[d_edge]
        incoming_edges = np.logical_and(col_inds == head, row_inds != tail)
        NBW[d_edge, incoming_edges] = 1.0
    return NBW, row_inds, col_inds

def vertex_marginals_from_incoming_messages(n_vertices, vrow_inds, vcol_inds):
    # creates linear transform for computing unnormalized vertex marginals from incoming messages
    mat_out = np.zeros((n_vertices, len(vrow_inds))) 
    for ind in range(n_vertices):
        incoming_edges = (vcol_inds == ind).astype(float)
        mat_out[ind, :] = incoming_edges
    return mat_out

def dedge_messages_from_vertices(n_vertices, vrow_inds, vcol_inds):
    # expands vertex marginals to dedge messages in a linear fashion
    n_edges = len(vrow_inds)
    mat_out = np.zeros((n_edges, n_vertices))
    for ind in range(n_vertices):
        outgoing_edges = (vrow_inds==ind).astype(float)
        mat_out[:, ind] = outgoing_edges
    return mat_out  



def denoiser(eta, rho):
    # expects (vector) input eta and scalar input rho

    # threshold eta to be within machine allowance
    tol = 700
    eta[eta > tol] = tol
    eta[eta < -tol] = -tol
    # now compute log cos as required
    return 0.5*np.log(np.cosh(eta+rho))-0.5*np.log(np.cosh(eta - rho))


def one_BP_experiment(n, p, d, lam, mu, max_BP_iters, small_init=0.1):
    
    # sample one dataset from model
    A, v = sbm_adjacency(n, d, lam)
    B, u = sbm_covariates(n, p, v, mu)
    rho = np.arctanh(lam/np.sqrt(d))
    rho_n = np.arctanh(lam*np.sqrt(d)/(n-d))
    
    # create required auxiliary matrices from dataset
    NBW, row_inds, col_inds = nonbacktracking_from_adj(A)
    v_from_E = vertex_marginals_from_incoming_messages(n, row_inds, col_inds)
    E_from_v = dedge_messages_from_vertices(n, row_inds, col_inds)
    mBg = B*np.sqrt(mu/gamma)
    mBg2 = mBg**2
    
    # initialize BP variables
    n_edges = len(row_inds)
    eta_vertices = np.random.randn(n) *small_init
    eta_vertices_prev = np.random.randn(n) *small_init
    eta_edges = E_from_v.dot(eta_vertices)
    m_cov = np.random.randn(p)*small_init
    m_cov_prev = np.random.randn(p)*small_init
    tau_inv = 1+mu - mBg2.dot(1/np.cosh(eta_vertices_prev)**2)
    norms = np.zeros(max_BP_iters)

    # run max_BP_iters iterations of BP
    for BP_iter in range(max_BP_iters):
        norms[BP_iter] = la.norm(eta_vertices)/np.sqrt(n)
        eta_vertices_new = mBg.T.dot(m_cov) - mBg2.T.dot(tau_inv)*np.tanh(eta_vertices_prev) \
                           + v_from_E.dot(denoiser(eta_edges, rho)) - np.ones(n)*np.sum(denoiser(eta_vertices, rho_n), axis=0)
        eta_edges_new = E_from_v.dot(mBg.T.dot(m_cov) - mBg2.T.dot(tau_inv)*np.tanh(eta_vertices_prev)) \
                            + NBW.dot(denoiser(eta_edges, rho)) - np.ones(n_edges) *np.sum(denoiser(eta_vertices, rho_n), axis=0)
        tau_inv_new = 1+ mu -mBg2.dot(1/np.cosh(eta_vertices)**2)
        m_cov_new = (mBg.dot(np.tanh(eta_vertices)) -mBg2.dot(1/np.cosh(eta_vertices)**2)*m_cov_prev)*tau_inv_new

        # now update the variables
        eta_vertices_prev = eta_vertices
        eta_vertices = eta_vertices_new
        eta_edges = eta_edges_new
        m_cov_prev = m_cov
        m_cov = m_cov_new
    
    
    results = {}
    # now compute whatever statistics you want
    results['hypothesis'] = float(norms[-1] > small_init)
    results['overlap_v'] = np.abs(np.sign(eta_vertices).dot(v)/n)
    results['overlap_u'] = np.abs(np.dot(m_cov/la.norm(m_cov), u/la.norm(u)))
    
    return results
    
def grid_BP_experiment(n, p, d, nMc, lamGrid, muGrid, statsToCompute):
    gamma = np.float(n)/np.float(p)
    nGridLam = len(lamGrid)
    nGridMu = len(muGrid)
    results = {}
    for stat in statsToCompute:
        results[stat] = np.zeros((nGridLam, nGridMu, nMc))
        

    count = nMc*len(lamGrid)*len(muGrid)
    for mc in range(nMc):
        for lamIn, lam in enumerate(lamGrid):
            for muIn, mu in enumerate(muGrid):
                t= time.time()
                exptResult = one_BP_experiment(n, p, d, lam, mu, 50)
                count = count - 1
                elapsed = time.time() - t
                if count%10 == 0 : 
                    print "Expected time remaining: {:.2f} hours".format(count*elapsed/3600.0) 
                for stat in statsToCompute:
                    results[stat][lamIn, muIn, mc] = exptResult[stat] 
    
    return results

if __name__=="__main__":
    
    # set global experiment parameters
    n = 800
    p = 1000
    gamma = np.float(n)/p
    d = 5.0
    nMc = 100

    # create parameter grid
    gridSpace = 30 
    lamGrid = np.linspace(0.001, 1, gridSpace)
    muGrid = np.linspace(0.001, np.sqrt(gamma), gridSpace)

    statsToCompute = ['hypothesis', 'overlap_v', 'overlap_u']

    results = grid_BP_experiment(n, p, d, nMc, lamGrid, muGrid, statsToCompute)
    
    path = './figs/'
    filename = 'phase_transition_dense.dat'
    pickle.dump( results, open( path+filename, "wb" ) )
    
